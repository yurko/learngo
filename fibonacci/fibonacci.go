package main

import "fmt"

// fibonacci is a function that returns
// a function that returns an int.
func fibonacci() func() int {
	f0 := 0
	f1 := 1
	i := 0

	return func() int {
		i++
		if i == 1 {
			return f0
		} else if i == 2 {
			return f1
		} else {
			fib := f0 + f1
			f0 = f1
			f1 = fib
			return fib
		}
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}
