package neuronetwork

import "math"

type Vector []float64

func (v Vector) Apply_activation() {
	for i, d := range v {
		v[i] = sigm(d)
	}

}

func sigm(x float64) float64 {
	return 1 / (1 + math.Pow(math.E, -x))
}
