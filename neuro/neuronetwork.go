package neuronetwork

import (
	"fmt"
	"math"
	"math/rand"
)

// NOTE: is map ok for it?
var Network = make(map[int]Matrix)

func initialize(input_num, hidden_num, output_num int) {
	// wrap input vector in matrix
	input_matrix := make(Matrix, 1)
	input_matrix[0] = make(Vector, input_num)

	// store input layer
	Network[0] = input_matrix

	neurons_per_layer := 4
	// for the 1st hidden layer height is eq to input neurons
	layer_height := input_num

	// initialize hidden layers
	for l := 1; l <= hidden_num; l++ {
		// pass neurons count (height of weights_matrix) to initialize
		weights_matrix := make(Matrix, neurons_per_layer)
		for i := 0; i < len(weights_matrix); i++ {
			weights_vector := make(Vector, layer_height)
			for i, _ := range weights_vector {
				weights_vector[i] = rand.Float64()
			}
			weights_matrix[i] = weights_vector
		}

		layer_height = neurons_per_layer
		Network[l] = weights_matrix
	}

	// initialize output layer
	weights_out := make(Matrix, output_num)
	for i := 0; i < len(weights_out); i++ {
		// calculate height of matrix based on last hidden layer
		output_height := len(Network[hidden_num])
		v_weights := make(Vector, output_height)
		for i, _ := range v_weights {
			v_weights[i] = rand.Float64()
		}
		weights_out[i] = v_weights
	}

	Network[hidden_num+1] = weights_out
}

func seed(input Vector) {
	for i, v := range input {
		Network[0][0][i] = v
	}
}

func Calculate(input Vector) Vector {
	seed(input)

	result := Network[0]
	for i := 1; i < len(Network); i++ {
		result = Dot(result, Network[i])
		result.Apply_activation()
	}

	// fmt.Println("-------")

	return result[0]
}

type TrainSet struct {
	input    Vector
	expected Vector
}

func train() {
	// 3 sets = 3 iteration
	train_sets := make([]TrainSet, 3)
	train_sets[0] = TrainSet{Vector{0, 0, 1, 1, 0, 1, 0, 1}, Vector{0, 1, 1, 0}}
	train_sets[1] = TrainSet{Vector{1, 0, 1, 1, 0, 0, 0, 1}, Vector{1, 0, 1, 0}}
	train_sets[2] = TrainSet{Vector{1, 1, 0, 0, 0, 1, 1, 1}, Vector{1, 0, 1, 1}}

	errors := make(Vector, 4)
	for i := 0; i < 10; i++ {
		for _, train_set := range train_sets {
			res := Calculate(train_set.input)

			for j, v := range res {
				errors[j] += math.Pow(train_set.expected[j]-v, 2)
			}

			for k, v := range errors {
				errors[k] = v / float64(len(train_sets))
			}
		}

		// Calculate deltas
		// deltas := make(Matrix, 3)

	}

	fmt.Println(errors)

}

func main() {
	v := Vector{1, 0, 0, 1, 1, 1, 1, 0}
	// 1, 0, 0, 1
	// 1, 1, 1, 0
	// 0, 1, 1, 1
	initialize(len(v), 2, 4)
	train()
	// fmt.Println(Calculate(v))
}
