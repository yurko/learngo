package neuronetwork

type Matrix []Vector

func (m Matrix) Apply_activation() {
	for _, v := range m {
		v.Apply_activation()
	}
}
