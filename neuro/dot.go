package neuronetwork

func Dot(left, right Matrix) Matrix {
	// C = L . R for an n × m matrix L and an m × p matrix R, then C is an n × p matrix

	n := len(left)
	m := len(right)
	p := len(right[0])

	c := make(Matrix, n)

	for i := 0; i < n; i++ {
		c[i] = make(Vector, p)
	}

	// Input: matrices L and R
	// Let C be a new matrix of the appropriate size
	// For i from 1 to n:
	//   For j from 1 to p:
	//     Let sum = 0
	//     For k from 1 to m:
	//       Set sum ← sum + Lik × Rkj
	//     Set Cij ← sum
	// Return C
	for i := 0; i < n; i++ {
		for j := 0; j < p; j++ {
			for k := 0; k < m; k++ {
				c[i][j] += left[i][k] * right[k][j]
			}
		}
	}

	return c
}
