package neuronetwork

import (
	"testing"

	"github.com/go-test/deep"
)

func TestDot(t *testing.T) {
	a := make(Matrix, 3)
	b := make(Matrix, 2)
	c := make(Matrix, 3)

	a[0] = Vector{2, 1}
	a[1] = Vector{-5, 6}
	a[2] = Vector{-4, 8}

	b[0] = Vector{1, -3, -5, 6}
	b[1] = Vector{0, 2, 1, -4}

	c[0] = Vector{2, -4, -9, 8}
	c[1] = Vector{-5, 27, 31, -54}
	c[2] = Vector{-4, 28, 28, -56}

	if diff := deep.Equal(Dot(a, b), c); diff != nil {
		t.Error(diff)
	}

	a = make(Matrix, 1)
	b = make(Matrix, 4)
	c = make(Matrix, 1)

	a[0] = Vector{2.3, -4.8, 1.1, -3.7}
	b[0] = Vector{-3.4, -2.2, 2.5, 3}
	b[1] = Vector{-4.8, 1.9, 7.6, 8.2}
	b[2] = Vector{-0.3, 1.3, 2.4, -6.2}
	b[3] = Vector{-1.8, 0.1, 2.7, -2.4}
	c[0] = Vector{21.55, -13.12, -38.08, -30.4}

	if diff := deep.Equal(Dot(a, b), c); diff != nil {
		t.Error(diff)
	}
}
